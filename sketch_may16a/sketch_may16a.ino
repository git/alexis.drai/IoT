// see https://github.com/kbeaugrand-org/ESP8266-AT-WIFI-MQTT

char c;

void setup() {
  Serial.begin(115200);
  delay(3000);
  do_connect();
  read_AT_response();
  create_tcp_server();
  read_AT_response();
}

bool AT_thingy(String thingy) {
  Serial.println(thingy);
  return true;
}

void read_AT_response() {
  while(Serial.available() > 0) 
  {
    c = Serial.read();
    Serial.print(c);
  }
  Serial.print('\n');  
}


void do_connect() {  
  while(!AT_thingy("AT+CWJAP=[\"Alexis\"],[\"M3rciAlexis!\"]"))
  {
    // wait
  }
  read_AT_response();
}

void is_connected() {
  while(!AT_thingy("AT+CWJAP?"))
  {
    // wait
  }
  read_AT_response();
}

void check_networks() {
  while(!AT_thingy("AT+CWLAP"))
  {
    //wait
  }
  read_AT_response();
}

void do_reconnect() {
  AT_thingy("AT+CWRECONNCFG=1");
  read_AT_response();
}

void create_tcp_server() {
  while(!AT_thingy("AT+CIPSERVER=1,8080"))
  {
    // wait
  }
  read_AT_response();
}

void is_tcp_server() {
  while(!AT_thingy("AT+CIPSERVER?"))
  {
    // wait
  }
  read_AT_response();
}

void about_tcp_connection() {
  while(!AT_thingy("AT+CIPSTATE?"))
  {
    // wait
  }
  read_AT_response();
}


void loop() {
  is_tcp_server();
  delay(1000);
  about_tcp_connection();
  delay(1000);
}
